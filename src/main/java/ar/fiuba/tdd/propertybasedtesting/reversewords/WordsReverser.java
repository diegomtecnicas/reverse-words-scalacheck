package ar.fiuba.tdd.propertybasedtesting.reversewords;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class WordsReverser {
    private static final List<String> SEPARATORS = asList(",", ";", "\\s");

    public static String reverseWords(String text) {
        return asStream(text).reduce("", (acc, input) -> isSeparator(input) ? acc + input : acc + reverse(input));
    }

    private static boolean isSeparator(String input) {
        return SEPARATORS.contains(input);
    }

    private static Stream<String> asStream(String input) {
        return Stream.of(input.split(buildRegExp()));
    }

    private static String buildRegExp() {
        String optionalSeparators = String.join("|", SEPARATORS);

        String matchSeparator = "(?=(" + optionalSeparators + "))";
        String matchWords = "(?<=(" + optionalSeparators + "))";

        return matchSeparator + "|" + matchWords;
    }

    private static String reverse(String input) {
        return new StringBuffer(input).reverse().toString();
    }
}
