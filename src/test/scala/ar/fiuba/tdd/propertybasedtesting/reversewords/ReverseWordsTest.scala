package ar.fiuba.tdd.propertybasedtesting.reversewords

import ar.fiuba.tdd.propertybasedtesting.reversewords.WordsReverser.reverseWords
import org.scalacheck.Gen.{alphaStr, listOf, oneOf}
import org.scalatest.PropSpec
import org.scalatest.prop.PropertyChecks

class ReverseWordsTest extends PropSpec with PropertyChecks {

  val inputGen = listOf(oneOf(oneOf(" ", ",", ";"), alphaStr))

  property("length of reversed input is the same as original") {
    forAll(inputGen) { (input: List[String]) =>
      val inputAsString = input.mkString("")
      assert(inputAsString.length == reverseWords(inputAsString).length)
    }
  }

  property("reversing twice should end with the same input") {
    forAll(inputGen) { (input: List[String]) =>
      val inputAsString = input.mkString("")
      assert(inputAsString == reverseWords(reverseWords(inputAsString)))
    }
  }

  property("each reversed word should be in original input") {
    forAll(inputGen) { (input: List[String]) =>
      val inputAsString = input.mkString("")
      val reversedWords = reverseWords(inputAsString)
      val reversedWordsList = input.filter(word => word.length > 1).map(word => word.reverse)

      for (reversedWord <- reversedWordsList) {
        assert(reversedWords.contains(reversedWord))
      }
    }
  }

}
